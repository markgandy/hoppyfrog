import styled from "styled-components";
import Link from "next/link";
import theme from "../../utils/theme";

const { colors } = theme;
const StyledButton = styled.button`
  width: 100%;
  background-color: ${colors.black};
  min-height: 68px;
  font-family: inherit;
  font-size: 15px;
  font-weight: bold;
  text-decoration: none;
  display: inline-flex;
  flex: auto;
  align-items: center;
  justify-content: center;
  white-space: nowrap;
  color: ${colors.amber};
  appearance: none;
  border: 0.3px solid ${colors.darkgray};
  &:hover {
    background-color: ${colors.darkgray};
  }
`;

const NavButton = ({ children, href }) => (
  <Link prefetch href={href}>
    <StyledButton as="a" href={href}>
      {children}
    </StyledButton>
  </Link>
);

export default NavButton;
