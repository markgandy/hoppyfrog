import React from "react";
import { Flex } from "rebass";
import Router, { withRouter } from "next/router";
import TopNavs from "./TopNavs";
import TwitterIcon from "react-svg-loader!../../svgs/twitter.svg";

class Toolbar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      navs: [{ href: "/", name: "Home" }, { href: "/blog", name: "Blog" }]
    };
    this.handleUpdateMenu = this.handleUpdateMenu.bind(this);
  }

  componentDidMount() {
    const { router } = this.props;
    this.handleUpdateMenu(router.pathname);
    Router.events.on("routeChangeComplete", this.handleUpdateMenu);
  }

  componentWillUnmount() {
    Router.events.off("routeChangeComplete", this.handleUpdateMenu);
  }

  handleUpdateMenu(href) {
    this.setState(state => {
      let navs = state.navs.map(nav => ({ ...nav }));
      navs = navs.map(nav => ({
        ...nav,
        isActive: href.substring(0, 4) === nav.href.substring(0, 4)
      }));
      return {
        navs
      };
    });
  }

  render() {
    const { navs } = this.state;
    return (
      <Flex flexWrap="wrap" alignItems="center" bg="black">
        <TopNavs navs={[navs[0], navs[1]]} />
        <Flex
          width={0.15}
          alignItems="center"
          justifyContent="flex-end"
          pr={[3, 4]}
        >
          <a href="https://twitter.com/mark_gand" target="_blank">
            <TwitterIcon as="a" height={25} />
          </a>
        </Flex>
      </Flex>
    );
  }
}

export default withRouter(Toolbar);
