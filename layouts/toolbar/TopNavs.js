import { Flex } from "rebass";
import NavButton from "./NavButton";
import theme from "../../utils/theme";

const { colors } = theme;

const TopNavs = ({ navs }) => (
  <Flex width={0.85} alignItems="center">
    {navs.map((nav, index) => {
      const width = [0.35, 0.3, 0.2];
      const children = nav.name;

      return (
        <Flex
          key={index}
          alignItems="center"
          justifyContent="center"
          width={width}
          style={{
            borderTop: nav.isActive ? `3px solid ${colors.amber}` : "none",
            paddingTop: nav.isActive ? "0" : "3px"
          }}
        >
          <NavButton href={nav.href} children={children} />
        </Flex>
      );
    })}
  </Flex>
);

export default TopNavs;
