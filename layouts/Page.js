import { Box } from "rebass";
import Toolbar from "./toolbar/Toolbar";

const Page = ({ children }) => (
  <Box>
    <Toolbar />
    <Box p={3} width={1} px={[3, 4, 5, 6, 7]}>
      {children}
    </Box>
  </Box>
);

export default Page;
