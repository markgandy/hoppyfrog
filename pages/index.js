import { Flex, Heading, Box } from "rebass";
import NextSeo from "next-seo";
import { ExternalLink } from "../components/Link";
import Bar from "../components/Bar";
import PrimaryButton from "../components/PrimaryButton";
import Passion from "../components/Passion";

function HomePage() {
  return (
    <React.Fragment>
      <NextSeo
        config={{
          title: "hoppyfrog - Home",
          description: "Home page for hoppyfrog blog.",
          canonical: "https://hoppyfrog.now.sh/"
        }}
      />
      <Box mb={4}>
        <Heading as="h1" my={4} fontSize={[4, 5]}>
          hoppyfrog 🐸
        </Heading>
        <Heading as="h1" my={4} fontSize={[3, 4]}>
          A collection of blog posts about a few random topics
        </Heading>
        <Flex my={4} alignItems="center" justifyContent="center">
          <PrimaryButton href="/blog" children="Visit Blog" />
        </Flex>
        <Bar />
        <Passion name="Beer">
          In particular craft beer from the smaller independent brewers - expect
          to see posts on anything from a hoppy pale ale to a rich chocolate
          porter. With a focus on the Australian beer scene, and where in
          Melbourne you can drink a tasty brew.
        </Passion>
        <Passion name="Football">
          Of course, Liverpool are by far the greatest team the world has ever
          seen - and our first ever blog post looks at LFC and their current
          Premier League title challenge.
        </Passion>
        <Passion name="Travel">
          Who doesn't love to travel? A couple of hoppyfrog contributors have
          recently been lucky enough to spend 10 months travelling around South
          America. Stay tuned for some upcoming posts about the trip.
        </Passion>
        <Passion name="Technology">
          This website was built with{" "}
          <ExternalLink href="https://nextjs.org/">Next.js</ExternalLink> and{" "}
          <ExternalLink href="https://rebassjs.org/">Rebass.js</ExternalLink>,
          and deployed using{" "}
          <ExternalLink href="https://zeit.co/now">Zeit Now</ExternalLink>.
          There may or may not be some boring posts about whatever the latest
          tech flavour of the month is.
        </Passion>
      </Box>
    </React.Fragment>
  );
}

export default HomePage;
