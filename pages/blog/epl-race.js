import { Heading, Text, Image } from "rebass";
import NextSeo from "next-seo";
import Table from "../../components/Table";
import { ExternalLink } from "../../components/Link";

const Paragraph = props => (
  <Text my={3} {...props}>
    {props.children}
  </Text>
);

export default () => (
  <React.Fragment>
    <NextSeo
      config={{
        title: "hoppyfrog - Blog Post - EPL Race",
        description:
          "Blog post from hoppyfrog looking at the Premier League title race 2018/2019.",
        canonical: "https://hoppyfrog.now.sh/blog/epl-race"
      }}
    />
    <Heading as="h1" my={3} fontSize={[3, 4]}>
      Premier League Title Race 2018/2019
    </Heading>
    <Heading as="h1" pb={3} fontSize={[1, 2]}>
      Fri 22 Mar, 2019 - Mark Gandy
    </Heading>
    <Image
      width={[1, 1, 2 / 3]}
      src="/static/liverpool-man-city.jpg"
      borderRadius={6}
      alt="Sadio Mané and Raheem Sterling chasing the ball"
    />
    <Paragraph>
      It's pretty clear that the Premier League title is now a two-horse race,
      after Tottenham's recent drop-off. So, as we head into the international
      break, I'm going to take a quick look at the incredible seasons that
      Liverpool and Manchester City have had up to this point, and try to figure
      out if The Reds can overcome City and win their first league title in 29
      years.
    </Paragraph>
    <Paragraph>
      This is the league table as we head into the last international break of
      the season, with Liverpool 2 points clear, having played a game more. City
      have a 6 better goal difference (and have scored more goals), so it looks
      likely they will come out on top if both teams finish on the same number
      of points.
    </Paragraph>
    <Table
      columns={4}
      data={[
        "Team",
        "Played",
        "Goal Diff",
        "Points",
        "Liverpool",
        "31",
        "+52",
        "76",
        "Man City",
        "30",
        "+58",
        "74"
      ]}
    />
    <Paragraph>
      To highlight how good they have been up to now, after their win against
      Fulham, Liverpool now have enough points to have won the 96/97 title
      (credit @BassTunedToRed). Manchester United won it that year with 75
      points, and with a 7 point cushion as well.
    </Paragraph>
    <Paragraph>
      Now lets project out to a full season, going at the same rate of points up
      to now, and see how they compare against the best ever Premier League
      seasons.
    </Paragraph>
    <Table
      columns={3}
      data={[
        "Season",
        "Team",
        "Points",
        "17/18",
        "Man City",
        "100",
        "04/05",
        "Chelsea",
        "95",
        "18/19",
        "Man City",
        "93.73",
        "18/19",
        "Liverpool",
        "93.16",
        "16/17",
        "Chelsea",
        "93",
        "05/06",
        "Chelsea",
        "91",
        "99/00",
        "Man United",
        "91"
      ]}
    />
    <Paragraph>
      Obviously it's not possible to get 93.73 or 93.16 points, but even if you
      round down to 93 then you can see that Man City and Liverpool this season
      are only bettered by Man City last year and Chelsea in 04/05. And of
      course, it assumes they can keep it going for the rest of the season, but
      there are only two seasons in the 27 year history of the Premier League
      where a team has gone at a faster rate than both Liverpool and Man City
      this season.
    </Paragraph>
    <Paragraph>
      There is a very good chance that whoever finishes second this season will
      do so with the highest ever points tally to not win the Premier League.
      Here are the highest points scoring runners up.
    </Paragraph>
    <Table
      columns={3}
      data={[
        "Season",
        "Team",
        "Points",
        "11/12",
        "Man United",
        "89",
        "08/09",
        "Liverpool",
        "86",
        "09/10",
        "Man United",
        "85",
        "07/08",
        "Chelsea",
        "85",
        "13/14",
        "Liverpool",
        "84"
      ]}
    />
    <Paragraph>
      The last time Liverpool won a league title was the 89/90 season, a little
      before the Premier League came together in 92/93. They have come close on
      a couple of occasions since then.
    </Paragraph>
    <Paragraph>
      <ul>
        <li>
          In the 08/09 season a Rafa Benitez side was spearheaded by a Gerrard /
          Torres forward line, and ably supported by the likes of Alonso,
          Mascherano and Hyppia (that was some team, maybe the best not to win
          the Premier League?)
        </li>
        <li>
          In 13/14 Brendan Rodgers came close with Luis Suarez as the star.
          Daniel Sturridge and Jordan Henderson were also phenomenal that year.
        </li>
      </ul>
    </Paragraph>
    <Paragraph>
      In those 2 seasons Liverpool came up against top quality opponents, as
      evidenced by the best losers above. In 08/09 Man United were champions
      with 90 points, and Man City won it with 86 points in 13/14.
    </Paragraph>
    <Paragraph>
      So can Liverpool break their duck and land the league title this year.
      Modelling a team's expected goals for and against is a relatively new way
      of rating teams and predicting match outcomes (it is becoming quite
      popular, and worth reading up on).{" "}
      <ExternalLink href="https://projects.fivethirtyeight.com/soccer-predictions/">
        FiveThirtyEight
      </ExternalLink>{" "}
      is one of those models, and based on their simulation the most likely
      outcome is for City to win the league by 2 points. Right now they give
      Liverpool a 34% chance of winning the league. However, this doesn't take
      into account City's gruelling schedule as they try for an unprecedented
      quadruple, so maybe it is more like 50/50.
    </Paragraph>
    <Table
      columns={4}
      data={[
        "Team",
        "SPI",
        "Points",
        "Win PL",
        "Man City",
        "94.7",
        "94",
        "66%",
        "Liverpool",
        "93.0",
        "92",
        "34%"
      ]}
    />
    <Paragraph>
      If Liverpool do miss out this year then they will likely be the first
      runners up to break 90 points (which will be zero consolation, obviously).
      But it does look like the club is in good shape to kick on and capture
      that elusive title in the next few years. Lets look back at the last two
      near-misses.
    </Paragraph>
    <Paragraph>
      <ul>
        <li>
          08/09 was an extremely good and balanced side, but the club was just
          about to implode under the reign of Hicks / Gillett. It's amazing the
          year they had with everything that was happening in the background.
        </li>
        <li>
          13/14 was a roller coaster season that Reds fans will remember
          forever. Some of the football was breathtaking, but most fans would
          admit it wasn't really sustainable. There was no solid foundation, and
          the "we can just score one more than you" approach is great until you
          can't.
        </li>
      </ul>
    </Paragraph>
    <Paragraph pb={5}>
      This current Liverpool side is built around a rock solid defence, and the
      club is as well run and engaged with the supporters as I can ever
      remember. So I am going to enjoy the run in - we have a real chance this
      year. But if we don't quite manage it, that's ok. Number 19 is coming
      soon.
    </Paragraph>
  </React.Fragment>
);
