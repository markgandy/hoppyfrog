import BlogCard from "../../components/BlogCard";
import NextSeo from "next-seo";

export default () => (
  <React.Fragment>
    <NextSeo
      config={{
        title: "hoppyfrog - Blog List",
        description: "Blog list page for hoppyfrog blog.",
        canonical: "https://hoppyfrog.now.sh/blog"
      }}
    />
    <BlogCard
      title="Premier League Title Race 2018/19"
      author="Mark Gandy"
      date="Fri 22 Mar, 2019"
      href="/blog/epl-race"
    >
      It's pretty clear that the Premier League title is now a two-horse race,
      after Tottenham's recent drop-off. So, as we head into the international
      break, I'm going to take a quick look at the incredible seasons that
      Liverpool and Manchester City have had up to this point, and try to figure
      out if The Reds can overcome City and win their first league title in 29
      years.
    </BlogCard>
  </React.Fragment>
);
