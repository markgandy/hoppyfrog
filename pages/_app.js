import React from "react";
import App, { Container } from "next/app";
import { ThemeProvider, createGlobalStyle } from "styled-components";
import theme from "../utils/theme";
import Page from "../layouts/Page";

export default class MyApp extends App {
  static async getInitialProps({ Component, router, ctx }) {
    let pageProps = {};

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }

    return { pageProps };
  }

  render() {
    const { Component, pageProps } = this.props;

    const Typography = createGlobalStyle`
      body {
        font-family: ${theme.fonts.sans};
        margin: 0;
      }
    `;

    return (
      <ThemeProvider theme={theme}>
        <Container>
          <Typography />
          <Page>
            <Component {...pageProps} />
          </Page>
        </Container>
      </ThemeProvider>
    );
  }
}
