import { Heading, Text } from "rebass";

const Passion = ({ name, children }) => (
  <React.Fragment>
    <Heading as="h1" mt={4} fontSize={[2, 3]}>
      {name}
    </Heading>
    <Text my={[2]} fontSize={[2, 3]}>
      {children}
    </Text>
  </React.Fragment>
);

export default Passion;
