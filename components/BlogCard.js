import { Card, Heading, Text, Flex } from "rebass";
import { InternalLink } from "./Link";
import theme from "../utils/theme";
import PrimaryButton from "./PrimaryButton";

const { colors } = theme;

const FlexCenter = props => (
  <Flex {...props} alignItems="center" justifyContent="center" />
);

const BlogCard = props => (
  <Card
    fontSize={[2, 3]}
    p={4}
    bg="lightgray"
    justifyContent="center"
    width={[1, 5 / 6]}
    my={3}
    borderRadius={8}
    boxShadow={`0 2px 16px ${colors.transparentBlack}`}
    {...props}
  >
    <Heading as="h1" my={1} fontSize={[3, 4]}>
      <InternalLink href={props.href}>{props.title}</InternalLink>
    </Heading>
    <Heading as="h1" my={1} fontSize={[1, 2]}>
    {props.date} - {props.author}
    </Heading>
    <Text my={3}>{props.children}</Text>
    <FlexCenter>
      <PrimaryButton href={props.href} children="Read More" />
    </FlexCenter>
  </Card>
);

export default BlogCard;
