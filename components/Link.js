import styled from "styled-components";
import NextLink from "next/link";
import { Link as RebassLink } from "rebass";
import theme from "../utils/theme";

const { colors } = theme;

const StyledLink = styled(RebassLink)`
  color: inherit;
  &:hover {
    color: ${colors.amber};
    transform: scale(1.0625);
  }
  &:focus {
    transform: scale(1.0625);
    box-shadow: ${colors.lightgray} 0px 0px 0px 2px,
      ${colors.cyan} 0px 0px 0px 4px;
    outline: none;
  }
`;

const InternalLink = ({ href, children }) => (
  <NextLink prefetch href={href}>
    <StyledLink as="a" href={href}>
      {children}
    </StyledLink>
  </NextLink>
);

const ExternalLink = ({ href, children }) => (
  <StyledLink as="a" href={href}>
    {children}
  </StyledLink>
);

export { InternalLink, ExternalLink };
