import styled from "styled-components";
import theme from "../utils/theme";
import gradient from "../utils/gradient";
import Link from "next/link";

const { colors } = theme;

const StyledButton = styled.button`
  font-family: inherit;
  font-size: 12px;
  font-weight: bold;
  text-decoration: none;
  margin: 0;
  padding: 12px;
  color: ${colors.lightgray};
  background-color: ${colors.red};
  background-image: ${gradient(120, "red", "amber")};
  border-width: 0;
  border-radius: 6px;
  border-style: initial;
  border-color: initial;
  border-image: initial;
  &:hover {
    color: ${colors.black};
    background-image: ${gradient(120, "lime", "cyan")};
    transform: scale(1.0625);
  }
  &:focus {
    transform: scale(1.0625);
    box-shadow: ${colors.lightgray} 0px 0px 0px 2px,
      ${colors.cyan} 0px 0px 0px 4px;
    outline: none;
  }
`;

const PrimaryButton = ({ href, children }) => (
  <Link prefetch href={href}>
    <StyledButton as="a" href={href}>
      {children}
    </StyledButton>
  </Link>
);

export default PrimaryButton;
