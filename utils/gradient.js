import theme from "./theme";

const { colors } = theme;
export default (n, from, to) =>
  `linear-gradient(${n}deg, ${colors[from] || from}, ${colors[to] || to})`;
