export default {
  // fontSizes: [12, 14, 16, 20, 24, 32, 48, 64],
  colors: {
    lightgray: "hsla(240, 100%, 98%, 1)",
    cyan: "hsla(180, 100%, 50%, 1)",
    lime: "hsla(88, 100%, 50%, 1)",
    black: "hsla(0, 0%, 0%, 1)",
    red: "hsla(338, 78%, 48%, 1)",
    amber: "hsla(32, 100%, 50%, 1)",
    darkgray: "hsla(258, 17%, 19%, 1)",
    transparentBlack: "hsla(0, 0%, 0%, 0.25)"
  },
  // space: [0, 4, 8, 16, 32, 64, 128, 256],
  fonts: {
    sans: "system-ui, sans-serif",
    mono: "Menlo, monospace"
  }
  // shadows: {
  //   small: "0 0 4px rgba(0, 0, 0, .125)",
  //   large: "0 0 24px rgba(0, 0, 0, .125)"
  // }
};
